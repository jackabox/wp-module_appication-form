<?php

/**
 * [bs_adtrak_applicant_table_head description]
 * @param  [type] $defaults [description]
 * @return $defaults        [description]
 */
function adtrak_applicant_table_head( $defaults ) {
    // clean up positioning 
    unset($defaults['title']);
    unset($defaults['date']);
    
    // If the user id  = 1, show id (Admin purposes)
    $user_id = get_current_user_id();
    if($user_id = 1) {
        $defaults['id'] = 'ID';
    }
    
    // set new defaults
    $defaults['title'] = 'Name';
    $defaults['location']  = 'Location';
    $defaults['status']  = 'Status';
    $defaults['date'] = 'Applied';
    return $defaults;
}
add_filter('manage_adtrak_applicants_posts_columns', 'adtrak_applicant_table_head');

/**
 * [adtrak_applicant_columns_content description]
 * @param  array    $column_name    detect the column name
 * @param  int      $post_ID        grab the post ID for editing
 * @return [type]              [description]
 */
function adtrak_applicant_columns_content($column_name, $post_ID) {
    if ($column_name == 'id') {
        echo $post_ID;
    }

    if ($column_name == 'location') {
        the_field('residence', $post_ID);
    }

    if ($column_name == 'status') {
        the_field('status', $post_ID);
    }
}
add_action('manage_adtrak_applicants_posts_custom_column', 'adtrak_applicant_columns_content', 10, 2);

/**
 * [sortable_adtrak_applicants_posts_columns description]
 * @param  [type] $columns [description]
 * @return [type]          [description]
 */
function sortable_adtrak_applicants_posts_columns( $columns ) {
    $columns['status'] = 'status'; 
    return $columns;
}
add_filter( 'manage_edit-adtrak_applicants_sortable_columns', 'sortable_adtrak_applicants_posts_columns' );


//restrict the posts by the chosen post format
function my_restrict_manage_posts() {
    global $typenow;

    if ($typenow == 'adtrak_applicants'){
        $field_key = "field_553fafc094dc6";
        $field = get_field_object($field_key)['choices'];

        ?>
        <select name="adtrak_applicants_filter">
        <option value=""><?php _e('All statuses', 'adtrak_applicants'); ?></option>
        <?php
            $current_v = isset($_GET['adtrak_applicants_filter']) ? $_GET['adtrak_applicants_filter'] : '';
            foreach ($field as $label => $value) {
                printf (
                    '<option value="%s"%s>%s</option>',
                    $value,
                    $value == $current_v? ' selected="selected"':'',
                    $label
                );
            }
        ?>
        </select>
        <?php
    }
}
add_action('restrict_manage_posts','my_restrict_manage_posts');

function request_applicant_filter($request) {    
    if (is_admin() && isset($request['post_type']) && $request['post_type']=='adtrak_applicants') {
        $request['meta_key'] = 'status';
        $request['meta_value'] = $_GET['adtrak_applicants_filter'];
    }
    return $request;
}
add_action( 'request', 'request_applicant_filter' );
