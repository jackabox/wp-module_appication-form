<?php 
if(isset($errors)) {
    echo '<ul>';
        foreach($errors as $value) {
            echo '<li>' . $value . '</li>';
        }
    echo '</ul>';
}
?>

<form id="application-form" enctype="multipart/form-data" class="application-form" name="custom-post-type" method="post" action="">
    <div class="error"></div>

    <div class="stage" id="stage-1">
        <p>The form consists of a few questions regarding personal details and then 10 freeform questions. It will take around 30 minutes to complete the application form and we recommend using a PC.</p>

        <p class="stage-info">Stage 1 of 4: Personal Details</p>

        <div class="row">
            <div class="half">
                <div class="float-label">
                    <label class="float" for="first_name">First name</label>
                    <input type="text" placeholder="What is your first name?" id="first_name" name="first_name" minlength="2" required>
                </div>
            </div><!-- /half -->
            
            <div class="half omega">
                <div class="float-label">
                    <label class="float" for="last_name">Last name</label>
                    <input type="text" placeholder="What is your last name?" id="last_name" name="last_name" minlength="2" required>
                </div>
            </div><!-- /half -->    
        </div><!-- /row -->
        
        <div class="row">
            <div class="third">
                <div class="float-label">
                    <label class="float" for="dob_day">Date of Birth</label>
                    <select id="dob_day" name="dob_day" required>
                        <option value="">Select Day</option>
                        <?php $i = 1; for($i; $i < 32; $i++) { ?>
                        <option value="<?php if($i < 10) { echo '0'; } echo $i; ?>"><?php if($i < 10) { echo '0'; } echo $i; ?></option>
                        <?php } ?>
                    </select>
                    <!-- <input type="text" placeholder="DD" minlength="2" maxlength="2" id="dob-day" name="dob-day" required> -->
                </div>
            </div>
            <div class="third">
                <div class="float-label">
                    <label class="float" for="dob_month"></label>
                    <select id="dob_month" name="dob_month" required>
                        <option value="">Select Month</option>
                        <option value='01'>January</option>
                        <option value='02'>February</option>
                        <option value='03'>March</option>
                        <option value='04'>April</option>
                        <option value='05'>May</option>
                        <option value='06'>June</option>
                        <option value='07'>July</option>
                        <option value='08'>August</option>
                        <option value='09'>September</option>
                        <option value='10'>October</option>
                        <option value='11'>November</option>
                        <option value='12'>December</option>
                    </select>
                </div>
            </div>
            <div class="third">
                <div class="float-label">
                    <label class="float" for="dob_year"></label>
                    <select id="dob_year" name="dob_year" required>
                        <option value="">Select Year</option>
                    <?php foreach(range(date('Y'), 1920) as $i) { ?>
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php } ?>
                    </select>
                </div>
            </div>
        </div>


        <div class="float-label">
            <label class="float" for="phone">Contact Number</label>
            <input type="tel" placeholder="What's your phone number?" id="phone" name="phone" minlength="6">
        </div>

        <div class="float-label">
            <label class="float" for="email">Email Address</label>
            <input type="email" placeholder="Got an email address?" id="email" name="email" required>
        </div>

        <div class="float-label">
            <label class="float" for="residence">Country of Residence</label>
            <select id="residence" name="residence" required>
                <option value="">Pick a Country</option>
                <?php 
                    $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
                    foreach($countries as $c) { ?>
                        <option value="<?php echo $c; ?>"><?php echo $c; ?></option>
                <?php } ?>
            </select>
        </div>

        <div class="float-label">
            <label class="float" for="image">Profile Picture</label> <br><br>
            <input type="file" name="image" id="image">
        </div>

        <br>
        
        <p class="stage-info">Stage 2 of 4: Social Links</p>
        
        <div class="row">
            <div class="half">
                <div class="float-label">
                    <label class="float" for="twitter">Twitter</label>
                    <input type="text" placeholder="Share your Twitter" id="twitter" name="twitter">
                </div>
            </div><!-- /half -->
            
            <div class="half omega">
                <div class="float-label">
                    <label class="float" for="facebook">Facebook</label>
                    <input type="text" placeholder="Share your Facebook" id="facebook" name="facebook">
                </div>
            </div><!-- /half -->    
        </div><!-- /row -->

        <div class="row">
            <div class="half">
                <div class="float-label">
                    <label class="float" for="linkedin">LinkedIn</label>
                    <input type="text" placeholder="Have you got LinkedIn?" id="linkedin" name="linkedin">
                </div>
            </div><!-- /half -->
            
            <div class="half omega">
                <div class="float-label">
                    <label class="float" for="blog">Blog</label>
                    <input type="text" placeholder="Have you got a blog?" id="blog" name="blog">
                </div>
            </div><!-- /half -->    
        </div><!-- /row -->

        <br>
          
        <button type="button" id="triggerSecond">Continue to the Next Step</button> 
    </div><!-- /stage1 -->

    <div class="stage" id="stage-2">
        <p class="stage-info">Stage 3 of 4: Questions 1-5</p>

        <div class="float-label">
            <label class="float" for="q1">In your opinion, what is your greatest accomplishment to date?</label>
            <textarea placeholder="" id="q1" name="q1" required></textarea>
            <div class="count">0/250</div>
        </div>

        <div class="float-label">
            <label class="float" for="q2">If you could change one thing about the world what would this be?</label>
            <textarea placeholder="" id="q2" name="q2" required></textarea>
            <div class="count">0/250</div>
        </div>

        <div class="float-label">
            <label class="float" for="q3">If you could change one thing about yourself, what would this be?</label>
            <textarea placeholder="" id="q3" name="q3" required></textarea>
            <div class="count">0/250</div>
        </div>

        <div class="float-label label-long">
            <label class="float" for="q4">If you were to travel to space and experience the world from this incredible perspective. What would this experience inspire you to do?</label>
            <textarea placeholder="" id="q4" name="q4" required></textarea>
            <div class="count">0/250</div>
        </div>

        <div class="float-label label-long">
            <label class="float" for="q5">The winner of the Rising Star initiative will receive mentoring from some of the world’s most iconic personalities. What do you hope to gain from this?</label>
            <textarea placeholder="" id="q5" name="q5" required></textarea>
            <div class="count">0/250</div>
        </div>

        <br>

        <button type="button" id="triggerThird">Continue to the Next Step</button> <span id="backFirst">or go back to the previous section</span>

    </div>

    <div class="stage" id="stage-3">
        <p class="stage-info">Stage 4 of 4: Questions 6-10</p>

        <div class="float-label">
            <label class="float" for="q6">Success – what does it mean to you?</label>
            <textarea placeholder="" id="q6" name="q6" required></textarea>
            <div class="count">0/250</div>
        </div>

        <div class="float-label">
            <label class="float" for="q7">What is your greatest fear?</label>
            <textarea placeholder="" id="q7" name="q7" required></textarea>
            <div class="count">0/250</div>
        </div>

        <div class="float-label">
            <label class="float" for="q8">What do you want your life legacy to be?</label>
            <textarea placeholder="" id="q8" name="q8" required></textarea>
            <div class="count">0/250</div>
        </div>

        <div class="float-label label-long">
            <label class="float" for="q9">How would you use the experience of going to space to inspire people to make a difference here on Earth?</label>
            <textarea placeholder="" id="q9" name="q9" required></textarea>
            <div class="count">0/250</div>
        </div>

        <div class="float-label">
            <label class="float" for="q10">How would you share/communicate the experience of going to space?</label>
            <textarea placeholder="" id="q10" name="q10" required></textarea>
            <div class="count">0/250</div>
        </div>

        <input type="hidden" name="post-type" id="post-type" value="adtrak_applicants" >
        <input type="hidden" name="action" value="adtrak_applicants"> 

        <?php wp_nonce_field('check_adtrak_application_nonce', 'adtrak_application_nonce'); ?>

        <br>
        
        <button type="submit" id="af_finish">All Done. Sign me up</button> <span id="backSecond">or go back to the previous section</span>
    </div>
</form>