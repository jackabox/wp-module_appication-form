<?php
/*
Plugin Name: Adtrak - Applicants
Plugin URI: http://www.adtrak.co.uk/
Description: Includes the applicant's view for the WordPress site.
Version: 0.1b
Author: Adtrak
Author URI: http://www.adtrak.co.uk/
License: 
Copyright: Adtrak LLP
*/

/**
 * Register the custom post type and fields
 */
function register_adtrak_applicants_post() {
 
    $labels = array(
        'name' => _x( 'Applicants', 'adtrak_applicants' ),
        'singular_name' => _x( 'Applicant', 'adtrak_applicants' ),
        'add_new' => _x( 'Add New', 'adtrak_applicants' ),
        'add_new_item' => _x( 'Add New Applicant', 'adtrak_applicants' ),
        'edit_item' => _x( 'Edit Applicant', 'adtrak_applicants' ),
        'new_item' => _x( 'New Applicant', 'adtrak_applicants' ),
        'view_item' => _x( 'View Applicant', 'adtrak_applicants' ),
        'search_items' => _x( 'Search Applicants', 'adtrak_applicants' ),
        'not_found' => _x( 'No applicants found', 'adtrak_applicants' ),
        'not_found_in_trash' => _x( 'No applicants found in Trash', 'adtrak_applicants' ),
        'parent_item_colon' => _x( 'Parent Applicant:', 'adtrak_applicants' ),
        'menu_name' => _x( 'Applicants', 'adtrak_applicants' ),
    );
 
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'description' => 'Applicants',
        'supports' => array( '' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 30,
        'menu_icon' => 'dashicons-admin-users',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'adtrak_applicants', $args );

    /**
     * Include ACF and ACF fields.
     */
    include('external/acf/acf.php');
    include('inc/acf_fields.php'); 
}
add_action( 'init', 'register_adtrak_applicants_post' );

/**
 * setting up the form area, to be called on the front end via a shortcode
 * @return adtrak-application-form
 */
function adtrak_application_form() { 
    if($_POST) {
        adtrak_application_save(); 
    }   

    include('inc/form.php');
}
add_shortcode('adtrak-application-form','adtrak_application_form');

if ( ! function_exists( 'wp_handle_upload' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
}


/**
 * Save application data. 
 * @return string returns string on fail
 * @return redirect if passes, redirects out.
 */
function adtrak_application_save() {
    $errors = array();
    $success = null;

    # check the nonce
    if (!isset($_POST) || !wp_verify_nonce($_POST['adtrak_application_nonce'], 'check_adtrak_application_nonce')) {
        $errors[] = 'Sorry, your nonce did not verify';
    } 

    # required fields
    $required_fields['first_name'] = 'Please enter a first name';
    $required_fields['last_name'] = 'Please enter a last name';
    $required_fields['email'] = 'Please enter your email address';
    $required_fields['residence'] = 'Please select a country of origin';

    # Loop through the fields and find any errors which may occur.
    foreach($_POST as $key => $value) {
        if(array_key_exists($key, $required_fields)) {
            if(trim($_POST[$key]) === '') {
                $errors[$key] = $required_fields[$key];
            }
        }
    }

    # Build the title from first/last name
    $first_name = sanitize_text_field(wp_strip_all_tags((isset($_REQUEST['first_name']) ? $_REQUEST['first_name'] : '')));
    $last_name  = sanitize_text_field(wp_strip_all_tags((isset($_REQUEST['last_name']) ? $_REQUEST['last_name'] : '')));
    $email      = sanitize_text_field(wp_strip_all_tags((isset($_REQUEST['email']) ? $_REQUEST['email'] : '')));
    $dobDay     = sanitize_text_field(wp_strip_all_tags((isset($_REQUEST['dob_day']) ? $_REQUEST['dob_day'] : '')));
    $dobMon     = sanitize_text_field(wp_strip_all_tags((isset($_REQUEST['dob_month']) ? $_REQUEST['dob_month'] : '')));
    $dobYear    = sanitize_text_field(wp_strip_all_tags((isset($_REQUEST['dob_year']) ? $_REQUEST['dob_year'] : '')));
    $phone      = sanitize_text_field(wp_strip_all_tags((isset($_REQUEST['phone']) ? $_REQUEST['phone'] : '')));
    $residence  = sanitize_text_field(wp_strip_all_tags((isset($_REQUEST['residence']) ? $_REQUEST['residence'] : '')));
    $twitter    = sanitize_text_field(wp_strip_all_tags((isset($_REQUEST['twitter']) ? $_REQUEST['twitter'] : '')));
    $facebook   = sanitize_text_field(wp_strip_all_tags((isset($_REQUEST['facebook']) ? $_REQUEST['facebook'] : '')));
    $linkedin   = sanitize_text_field(wp_strip_all_tags((isset($_REQUEST['linkedin']) ? $_REQUEST['linkedin'] : '')));
    $tumblr     = sanitize_text_field(wp_strip_all_tags((isset($_REQUEST['tumblr']) ? $_REQUEST['tumblr'] : '')));
    $blog       = sanitize_text_field(wp_strip_all_tags((isset($_REQUEST['blog']) ? $_REQUEST['blog'] : '')));
    $q1         = sanitize_text_field((isset($_REQUEST['q1']) ? $_REQUEST['q1'] : ''));
    $q2         = sanitize_text_field((isset($_REQUEST['q2']) ? $_REQUEST['q2'] : ''));
    $q3         = sanitize_text_field((isset($_REQUEST['q3']) ? $_REQUEST['q3'] : ''));
    $q4         = sanitize_text_field((isset($_REQUEST['q4']) ? $_REQUEST['q4'] : ''));
    $q5         = sanitize_text_field((isset($_REQUEST['q5']) ? $_REQUEST['q5'] : ''));
    $q6         = sanitize_text_field((isset($_REQUEST['q6']) ? $_REQUEST['q6'] : ''));
    $q7         = sanitize_text_field((isset($_REQUEST['q7']) ? $_REQUEST['q7'] : ''));
    $q7         = sanitize_text_field((isset($_REQUEST['q7']) ? $_REQUEST['q7'] : ''));
    $q8         = sanitize_text_field((isset($_REQUEST['q8']) ? $_REQUEST['q8'] : ''));
    $q9         = sanitize_text_field((isset($_REQUEST['q9']) ? $_REQUEST['q9'] : ''));
    $q10        = sanitize_text_field((isset($_REQUEST['q10']) ? $_REQUEST['q10'] : ''));

    if(!is_email($email)) {
        $errors[] = 'The email you entered is not valid.';
    }

    # if we have caught errors, return them on the page, else add this stuff!
    if(!empty($errors)) {
        echo '<h5>Please fix the following errors</h5>';
        echo '<ul class="form-errors">';
            foreach($errors as $value) {
                echo '<li>' . $value . '</li>';
            }
        echo '</ul>';

    } else {
        # add the content of the form to $post as an array
        $post = array(
            'post_title' => $first_name . ' ' . $last_name,
            'post_content' => '',
            'post_category' => '',
            'tags_input' => '',
            'post_status' => 'publish', // Choose: publish, preview, future, etc
            'post_type' => $_POST['post-type'] // Use a custom post type if you want to
        );

        $id = wp_insert_post($post); // http://codex.wordpress.org/Function_Reference/wp_insert_post

        # Set the user: meta data.
        update_field('field_553fac8294db0', $first_name, $id);
        update_field('field_553fac9194db1', $last_name, $id);
        update_field('field_553faced94db2', $email, $id); 
        update_field('field_553facfa94db3', $phone, $id);
        update_field('field_553fad0c94db4', $dobDay.'/'.$dobMon.'/'.$dobYear, $id);
        update_field('field_553fb04c0e9fd', $residence, $id);

        # Set the user: social data.
        update_field('field_553fad2294db5', $twitter, $id);
        update_field('field_553fad2d94db6', $facebook, $id);
        update_field('field_553fae6094db9', $linkedin, $id);
        update_field('field_553fae2a94db7', $blog, $id);
        
        # Set the user: questions data.
        update_field('field_553fae7594dba', $q1, $id);
        update_field('field_553fae9a94dbb', $q2, $id);
        update_field('field_553faeb894dbc', $q3, $id);
        update_field('field_553faed494dbd', $q4, $id);
        update_field('field_553faeea94dbe', $q5, $id);
        update_field('field_553faf0e94dbf', $q6, $id);
        update_field('field_553faf2894dc0', $q7, $id);
        update_field('field_553faf3394dc1', $q8, $id);
        update_field('field_553faf4c94dc2', $q9, $id);
        update_field('field_553fb43dff045', $q10, $id);

        # Just for predefined defaults
        update_field('field_553fafc094dc6','Pending',$id);
       
        $file   = $_FILES['image'];
        $file['name'] = mt_rand() . '-' . $file['name'];

        $allowed =  ['gif','png','jpg','jpeg'];
        $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
       
        if(in_array($ext,$allowed) ) {
            $upload = wp_handle_upload($file, array('test_form' => false));
            update_field('field_553faf8c94dc4', $upload['url'], $id);
        }

        # Email the kruger team informing them  
        $to = "jack.whiting@adtrak.co.uk";
        $subject = "New Applicaton Received"; 
        $body = "You have recieved a new application for the rising star programme and it is ready to be reviewed at your admin panel. Please visit <a href='http://risingstarprogramme.com/wp-admin'>http://risingstarprogramme.com/wp-admin</a> to review the application.";
        mail($to, $subject, $body);

        # redirect location + exit
        $location = home_url('application/thanks');
        echo "<meta http-equiv='refresh' content='0;url=$location' />";
        exit;
    } //endif
}

/* 
    Include the admin stuff
*/
include_once('inc/admin.php');

